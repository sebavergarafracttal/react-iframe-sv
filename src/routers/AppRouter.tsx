import { Routes, Route, BrowserRouter } from "react-router-dom";
import { IframeChild } from "../components/IframeChild";
import { IframeParent } from "../components/IframeParents";
import { PublicRoute } from "./PublicRoute";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/iframe"
          element={
            <PublicRoute>
              <IframeParent />
            </PublicRoute>
          }
        />
        <Route
          path="/iframe-child"
          element={
            <PublicRoute>
              <IframeChild />
            </PublicRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};
