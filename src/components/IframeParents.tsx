export const IframeParent = () => {
  return <>
    <h1>Iframe Parent</h1>;
    <iframe
      src="/iframe-child"
      width="600"
      height="300"
      title="Child Iframe"
    >
    </iframe>
  </>
};
